# Somos PNT Test Utils
##### Utilidades generales para tests

## Qué es Somos PNT Test Utils?
Test Utils es una librería con utilidades genéricas para tests.

## Instalación

```xml
<dependency>
    <groupId>com.somospnt</groupId>
    <artifactId>test-utils</artifactId>
    <version>1.3.0</version>
    <scope>test</scope>
</dependency>     
```

## Uso de Builders

```java
 //construir un objeto en memoria
 Cliente mo1 = new ClienteBuilder()
     .habilitadoParaComprar()
     .setNumeroDocumento("30454212")
     .setMiembroDesde(LocalDate.now())
     .build();

 //construir un objeto en memoria y persistirlo
 Cliente mo1 = new ClienteBuilder()
     .deshabilitado()
     .build(entityManager);
     
 //Crear un mock de una operacion de servicio REST
  MockRestServiceServer mockRestServiceServer = new BusquedaClienteMockRestOperation()
     .clientesEncontrados(miListaDeClientes)
     .mock(restTemplate);

  ...
  
  mockRestServiceServer.verify();

  MockRestServiceServer mockRestServiceServer = new BusquedaClienteMockRestOperation()
     .errorInternoDelServidor()
     .mock(restTemplate);

  ...

  mockRestServiceServer.verify();

//Crear un mock de una secuencia de operaciones de servicios REST
 MockRestServiceServer mockRestServiceServer = MockRestServiceServerBuilder
    .withRestTemplate(restTemplate)
    .withMockOperation(new MockRestGetOperationImpl("URL1", HttpStatus.ACCEPTED))
    .withMockOperation(new MockRestGetOperationImpl("URL2", HttpStatus.CREATED))
    .build();

 ...

 mockRestServiceServer.verify();
```

## Licencia
Somos PNT Test Utils se distribuye bajo Mozilla Public License, version 2.0.
El archivo LICENSE contiene más información sobre la licencia de este producto.
Puede leer más sobre MPL en [Mozilla Public License FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/).