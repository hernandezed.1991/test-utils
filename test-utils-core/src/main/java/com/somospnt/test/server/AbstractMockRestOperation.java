/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.test.server;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.test.web.client.ResponseCreator;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import org.springframework.web.client.RestTemplate;

/**
 * An abstract class that creates mocks for a single REST operation.
 */
public abstract class AbstractMockRestOperation {

    /**
     * The mock server to use.
     */
    protected MockRestServiceServer mockServer;
    /**
     * The HTTP Status for the response. This value is used by the default
     * startMockServer() method to prepare the mockServer.
     */
    protected HttpStatus status;

    /**
     * Returns the URL for this REST operation.
     *
     * @return the URL.
     */
    protected abstract String getUrl();

    /**
     * Returns the HTTP method (GET, POST, etc) for this REST operation.
     *
     * @return the HTTP method to use.
     */
    protected abstract HttpMethod getHttpMethod();

    /**
     * Returns a ResponseCreator to use if the status is 2xx.
     *
     * @return a ResponseCreator with the success response.
     */
    protected abstract ResponseCreator getSuccessResponse();

    /**
     * Returns a ResponseCreator to use if the status is NOT 2xx.
     *
     * @return a ResponseCreator with the error response.
     */
    protected ResponseCreator getErrorResponse() {
        return withStatus(status);
    }

    /**
     * Returns the expected request to match.
     *
     * @return a RequestMatcher with the expression to match the incoming
     * request.
     */
    protected RequestMatcher getExpect() {
        return requestTo(getUrl());
    }

    /**
     * Starts the mockServer and prepares it for mocking an operation.
     *
     * @param restTemplate the RestTemplate to use for creating the mockServer.
     */
    public MockRestServiceServer mock(RestTemplate restTemplate) {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mock(mockServer);
        return mockServer;
    }

    /**
     * Adds an operation to mock server
     */
    void mock(MockRestServiceServer mockServer) {
        if (isSuccess(status)) {
            mockServer.expect(method(getHttpMethod()))
                    .andExpect(getExpect())
                    .andRespond(getSuccessResponse());
        } else {
            mockServer.expect(method(getHttpMethod()))
                    .andExpect(getExpect())
                    .andRespond(getErrorResponse());
        }
    }

    /**
     * Determines is the given status is successful or not.
     *
     * @param status the HttpStatus to check.
     * @return true if it is an 2xx status, false otherwise.
     */
    protected boolean isSuccess(HttpStatus status) {
        return status != null && status.is2xxSuccessful();
    }

}
