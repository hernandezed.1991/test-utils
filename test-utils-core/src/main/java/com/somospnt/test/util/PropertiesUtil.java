/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.test.util;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple util class for reading property values from different sources. 
 * The builder methods for this class always returns the same instance for 
 * each resource.
 */
public class PropertiesUtil {

    private static final Map<String, PropertiesUtil> propertiesMap = new HashMap<>();
    private Properties properties;

    private PropertiesUtil() {
    }

    /** Loads properties from file "application.properties" in classpath. 
     * @return an instance with properties loaded from "application.properties"
     * @throws RuntimeException if the file does not exist.
     * file. This method will always return the same instance.
     */
    public static PropertiesUtil fromSpringApplication() {
        return from("application.properties");
    }

    /** Loads a properties file. This file content is cached, and future 
     * invocations to this method will return the same instance.
     * @param resource the location of the resource to load properties from.
     * @return an instance with properties loaded from the given resource.
     * This method will always return the same instance.
     * @throws RuntimeException if the file does not exist.
     */
    public synchronized static PropertiesUtil from(String resource) {
        PropertiesUtil propertiesUtil = propertiesMap.get(resource);
        if (propertiesUtil == null) {
            propertiesUtil = new PropertiesUtil();
            propertiesUtil.properties = loadProperties(resource);   
            propertiesMap.put(resource, propertiesUtil);
        }
        return propertiesUtil;
    }

    private static Properties loadProperties(String resource) {
        Properties prop = new Properties();
        try {
            prop.load(PropertiesUtil.class.getClassLoader().getResourceAsStream(resource));
        } catch (Exception ex) {
            throw new RuntimeException("Can't load file: " + resource, ex);
        }
        return prop;
    }
    
    /** 
     * Returns the value for the given property.
     * @param key the key to search.
     * @return the value for the given property.
     * @throws NoSuchElementException if the key is not found.
     */
    public String getProperty(String key) {
        String value = properties.getProperty(key);
        if (value == null) {
            throw new NoSuchElementException("Can't find property: " + key);
        }
        return value;
    }  
    
}
